<?php

namespace app\themes\mintos;

use Yii;
use yii\base\Theme as YiiTheme;

/**
 * Esta Clase muestra contenidos HTML con bootstrap
 * @package ticmakers\yii2-themes\mintos
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class Theme extends YiiTheme
{
    private $_mainLayout = 'main';
    private $_pathMap = ['@app/views' => '@themes/mintos/views'];
    private $_imagesBaseUrl = '@web/images';

    /**
     * Get the value of _mainLayout
     */
    public function getMainLayout()
    {
        return $this->_mainLayout;
    }

    /**
     * Set the value of _mainLayout
     *
     * @return  self
     */
    public function setMainLayout($mainLayout)
    {
        $this->_mainLayout = $mainLayout;

        return $this;
    }

    /**
     * Get the value of _imagesBaseUrl
     */
    public function getImagesBaseUrl()
    {
        return $this->_imagesBaseUrl;
    }

    /**
     * Set the value of _imagesBaseUrl
     *
     * @return  self
     */
    public function setImagesBaseUrl($imagesBaseUrl)
    {
        $this->_imagesBaseUrl = $imagesBaseUrl === null ? null : rtrim(Yii::getAlias($imagesBaseUrl), '/');

        return $this;
    }

    /**
     * Converts a relative URL into an absolute URL using [[baseUrl]].
     * @param string $url the relative URL to be converted.
     * @return string the absolute URL
     * @throws InvalidConfigException if [[baseUrl]] is not set
     */
    public function getImageUrl($url)
    {
        if (($baseUrl = $this->getImagesBaseUrl()) !== null) {
            return $baseUrl . '/' . ltrim($url, '/');
        }

        throw new InvalidConfigException('The "imagesBaseUrl" property must be set.');
    }
}
