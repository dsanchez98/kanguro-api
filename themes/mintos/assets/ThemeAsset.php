<?php

namespace app\themes\mintos\assets;

use yii\web\AssetBundle;

/**
 * 
 */
class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@themes/mintos/base/';
    public $css = [
        'vendors/jquery-toggles/css/toggles.css',
        'vendors/jquery-toggles/css/themes/toggles-light.css',
        'vendors/jquery-toast-plugin/dist/jquery.toast.min.css',
        'dist/css/style.css'
    ];
    public $js = [
        'dist/js/jquery.slimscroll.js',
        'dist/js/feather.min.js',
        'vendors/jquery-toast-plugin/dist/jquery.toast.min.js',
        'vendors/tinymce/tinymce.min.js',
        'dist/js/init.js'
    ];
    public $depends = [];
}
