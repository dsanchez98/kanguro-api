<?php

namespace app\themes\mintos;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package ticmakers\yii2-themes
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 */
class Bootstrap implements BootstrapInterface
{
    const BS4_ASSET = 'yii\bootstrap4\BootstrapAsset';
    const BSP4_ASSET = 'yii\bootstrap4\BootstrapPluginAsset';
    const JQUERY_ASSET = 'yii\web\JqueryAsset';
    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {

        $baseUrl = Yii::$app->assetManager->getPublishedUrl('@themes/mintos/base/');
        $app->getView()->theme->setBaseUrl($baseUrl);
        $bundles = $app->getAssetManager()->bundles;

        if (!isset($bundles[static::BS4_ASSET])) {
            $app->getAssetManager()->bundles[static::BS4_ASSET] = [
                'sourcePath' => '@themes/mintos/base',
                'css' => [
                    'vendors/bootstrap/dist/css/bootstrap.min.css',
                ],
            ];
        }

        if (!isset($bundles[static::BSP4_ASSET])) {
            $app->getAssetManager()->bundles[static::BSP4_ASSET] = [
                'sourcePath' => '@themes/mintos/base',
                'js' => [
                    'vendors/popper.js/dist/umd/popper.min.js',
                    'vendors/bootstrap/dist/js/bootstrap.min.js',
                ],
            ];
        }
        if (!isset($bundles[static::JQUERY_ASSET])) {
            $app->getAssetManager()->bundles[static::JQUERY_ASSET] = [
                'sourcePath' => '@themes/mintos/base',
                'js' => [
                    'vendors/jquery/dist/jquery.min.js'
                ],
            ];
        }
        \Yii::setAlias('@mintos', '@app/themes/mintos');
        \Yii::setAlias('@theme', '@app/themes/mintos');
        $this->overrideViews($app);
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideViews($app)
    {
        $app->getView()->theme->pathMap['@theme/views'] = "@app/views";
    }
}
