<?php

use ticmakers\core\widgets\Menu;

use ticmakers\core\widgets\Breadcrumbs;

$items = [
    [
        'label' => '<span class="feather-icon"><i data-feather="search"></i></span>',
        'url' => 'javascript:void(0);',
        'template' => '<a id="navbar_search_btn" class="nav-link nav-link-hover" href="{url}">{label}</a>'
    ],
    [
        'label' => '<span class="feather-icon mr-5"><i data-feather="plus-circle"></i></span>',
        'url' => 'javascript:void(0);',
        'template' => '<a id="settings_toggle_btn" class="nav-link nav-link-hover" href="{url}">{label} Crear</a>'
    ],
    [
        'label' => Yii::t('app', 'Login'),
        'url' => ['//adm/security/login'],
        'template' => '<a href="{url}">{label}</a>', // '<a class="load-modal" href="{url}">{label}</a>',
        'visible' => Yii::$app->user->isGuest
    ],
    [
        'label' => Yii::t('app', 'Register'),
        'url' => ['//adm/security/register'],
        'template' => '<a href="{url}">{label}</a>', // '<a class="load-modal" href="{url}">{label}</a>',
        'visible' => Yii::$app->user->isGuest
    ],
    [
        'label' => Yii::t('app', 'Logout'),
        'url' => ['//adm/security/logout'],
        'template' => '<a data-method="post" href="{url}">{label}</a>',
        'visible' => !Yii::$app->user->isGuest
    ],
];
?>


<nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">
    <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>
    <a class="navbar-brand" href="<?= Yii::$app->homeUrl ?>" style="min-width: 170px; max-width: 170px;">
        <img class="brand-img d-inline-block" src="<?= $this->theme->getImageUrl('logo.png') ?>" alt="<?= Yii::$app->name ?>" />
    </a>

    <div>
        <h6 class="ml-15 mt-10" style="line-height: 1rem;"><?= $this->moduleTitle ?? $this->title ?></h6>
        <!-- Breadcrumb -->
        <nav class="hk-breadcrumb" aria-label="breadcrumb">
            <?= Breadcrumbs::widget([
                'tag' => 'ol',
                'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>\n",
                'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
                'options' => ['class' => 'breadcrumb breadcrumb-light bg-transparent mb-0'],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
            ]) ?>
        </nav>
        <!-- /Breadcrumb -->
    </div>

    <?php
    echo Menu::widget([
        'items' => $items,
        'options' => ['class' => 'navbar-nav hk-navbar-content'],
        'renderInnerContainer' => false,
        'itemOptions' => [
            'class' => 'nav-link nav-link-hover'
        ]
    ]);
    ?>

</nav>

<form role="search" class="navbar-search">
    <div class="position-relative">
        <a href="javascript:void(0);" class="navbar-search-icon"><span class="feather-icon"><i data-feather="search"></i></span></a>
        <input type="text" name="example-input1-group2" class="form-control" placeholder="Type here to Search">
        <a id="navbar_search_close" class="navbar-search-close" href=""><span class="feather-icon"><i data-feather="x"></i></span></a>
    </div>
</form>

<?php 
    $this->registerJs("
            
    ")
?>