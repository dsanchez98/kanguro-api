<?php

use ticmakers\core\widgets\Menu;
use app\models\app\Menus;

$menu = Menus::find()
    ->where([
        'active' => Menus::STATUS_ACTIVE,
        'position' => 'vertical-nav'
    ])
    ->one();
?>

<?php
$submenuTemplate = <<<HTML
<a class="nav-link collapsed" {data-toggle} {data-target-submenu} aria-expanded="false">
    <span class="feather-icon"><i data-feather="{icon}"></i></span> <span class="nav-link-text">{label}</span>
</a>
<div id="{id-submenu}" class="collapse {show}">
    <ul class="nav flex-column ml-3">{items}</ul>
</div>
HTML;
?>

<nav class="hk-nav hk-nav-dark">
    <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <?= Menu::widget([
                'renderInnerContainer' => false,
                'options' => [
                    'class' => 'navbar-nav flex-column'
                ],
                'items' => (!empty($menu) ? $menu->getItems() : []),
                'linkTemplate' => '<a data-method="post" class="nav-link {active}" href="{url}" {data-toggle} {data-target-submenu}><span class="feather-icon"><i data-feather="{icon}"></i></span> <span class="nav-link-text">{label}</span></a>',
                'submenuTemplate' => $submenuTemplate
            ]); ?>
        </div>
    </div>
</nav>

<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>