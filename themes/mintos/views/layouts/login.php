<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use ticmakers\core\base\Modal;
use yii\bootstrap4\Html;

AppAsset::register($this);
$this->registerJs("
    window.TEXT_EMPTY = '" . Yii::$app->strings::getTextEmpty() . "';
    let loadModalElements = document.getElementsByClassName('load-modal');
    for (let element of loadModalElements) {
        element.addEventListener('click', function (evt) {
            evt.preventDefault();
        });
    }
", \yii\web\View::POS_LOAD);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->


    <!-- HK Wrapper -->
    <div class="hk-wrapper">

        <!-- Main Content -->
        <div class="hk-pg-wrapper hk-auth-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-5 pa-0">
                        <div class="fadeOut item auth-cover-img overlay-wrap" style="background-image:url(<?= $this->theme->getImageUrl('bg-login.jpg') ?>);">
                            <div class="auth-cover-info py-xl-0 pt-100 pb-50">
                                <div class="auth-cover-content text-center w-xxl-75 w-sm-90 w-xs-100">
                                    <h1 class="display-3 text-white mb-20">Understand and look deep into nature.</h1>
                                    <p class="text-white">The purpose of lorem ipsum is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn't distract from the layout. Again during the 90s as desktop publishers bundled the text with their software.</p>
                                </div>
                            </div>
                            <div class="bg-overlay bg-trans-dark-50"></div>
                        </div>
                    </div>
                    <div class="col-xl-7 pa-0">



                        <div class="auth-form-wrap py-xl-0 py-50">
                            <div class="auth-form w-xxl-55 w-xl-75 w-sm-90 w-xs-100">

                                <header class="d-flex justify-content-center align-items-center">
                                    <a class="d-flex auth-brand w-100 px-100 py-30" href="#">
                                        <img class="brand-img " src="<?= $this->theme->getImageUrl('logo.png') ?>" alt="<?= Yii::$app->name ?>" />
                                    </a>
                                </header>

                                <!-- Container -->
                                <div class="container">
                                    <?= $content ?>
                                </div>
                                <!-- /Container -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Main Content -->

    </div>
    <!-- /HK Wrapper -->
    <?php Modal::begin([
        'id' => 'default-modal',
        'title' => Yii::t('app', '{title}'),
        'size' => Modal::SIZE_DEFAULT,
        'options' => [
            'style' => 'display: none;',
            'tabindex' => false,
        ],
        'clientOptions' => [
            'backdrop' => 'static',
            'keyboard' => false,
        ],
    ]);
    Modal::end();
    ?>
    <div id="move-modal"></div>
    <?php $this->endBody() ?>
</body>
<?php $this->endPage() ?>