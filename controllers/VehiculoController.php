<?php

namespace app\controllers;

use app\base\rest\ActiveController;

 /**
 * VehiculosController Clase encargada de presentar y manipular la información del modelo Vehiculo para las solicitudes en el api
 *
 * @package app/modules
 * @subpackage rest/controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 */
class VehiculoController extends ActiveController
{

    /**
     * Modelo para las operaciones CRUD
     * @var string
     */
    public $modelClass = 'app\models\base\Vehiculo';

    /**
     * Modelo para las búsquedas
     * @var string
     */
    public $searchModel = 'app\models\api\Vehiculo';

    /**
     * Llave primaria del modelo para la sincronización
     * @var string
     */
    public $primaryKey = 'vehiculo_id';

}
