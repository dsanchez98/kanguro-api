<?php

namespace app\controllers;

use app\base\rest\ActiveController;

 /**
 * AsignacionsController Clase encargada de presentar y manipular la información del modelo Asignacion para las solicitudes en el api
 *
 * @package app\controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 */
class AsignacionController extends ActiveController
{

    /**
     * Modelo para las operaciones CRUD
     * @var string
     */
    public $modelClass = 'app\models\base\Asignacion';

    /**
     * Modelo para las búsquedas
     * @var string
     */
    public $searchModel = 'app\models\api\Asignacion';

    /**
     * Llave primaria del modelo para la sincronización
     * @var string
     */
    public $primaryKey = 'asignacion_id';

}
