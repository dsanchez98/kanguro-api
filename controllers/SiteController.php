<?php

namespace app\controllers;

use app\models\base\Asignacion;
use app\models\base\Persona;
use app\models\base\Vehiculo;
use Yii;
use ticmakers\adm\modules\api\controllers\behaviors\LanguageBehavior;
use ticmakers\base\rest\actions\ErrorAction;

/**
 *
 * Implementación de todas las funcionalidad de seguridad para el API.
 *
 * @package app
 * @subpackage modules\api\controllers
 * @category controllers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 * @since 2.0.0
 */
class SiteController extends \app\base\rest\Controller
{
    const ACTION_INDEX = 'index';

    public $defaultAction = self::ACTION_INDEX;

    /**
     * Undocumented function
     *
     * @return void
     */
    public function actions()
    {
        return [
            'error' => ErrorAction::class
        ];
    }

    /**
     * Retorna la lista de behaviors que el controlador implementa
     *
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator'], $behaviors['verbFilter']);

        $behaviors['language'] = LanguageBehavior::class;

        return $behaviors;
    }

    /**
     * Validación de señal
     */
    public function actionValidateConection()
    {
        return [
            'message' => Yii::t('adm', 'Online')
        ];
    }

    /**
     * 
     */
    public function actionIndex()
    {
        return Yii::$app->name . ' Api';
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function actionHome()
    {

        return [
            'vehiculos' => Vehiculo::find()->count(),
            'personas' => Persona::find()->count(),
            'asignaciones' => Asignacion::find()->count()
        ];
    }
}
