<?php

namespace app\controllers;

use app\base\rest\ActiveController;
 /**
 * PersonasController Clase encargada de presentar y manipular la información del modelo Persona para las solicitudes en el api
 *
 * @package app\controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 */
class PersonaController extends ActiveController
{

    /**
     * Modelo para las operaciones CRUD
     * @var string
     */
    public $modelClass = 'app\models\base\Persona';

    /**
     * Modelo para las búsquedas
     * @var string
     */
    public $searchModel = 'app\models\api\Persona';

    /**
     * Llave primaria del modelo para la sincronización
     * @var string
     */
    public $primaryKey = 'persona_id';

}
