<?php

namespace app\models\api;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\base\Ciudad as CiudadModel;

/**
 * Ciudad represents the model behind the search form about `app\models\base\Ciudad`.
 */
class Ciudad extends CiudadModel
{

    /**
     * Permite establecer el número de registros a ser mostrados por pagina
     * @var type
     */
    public $perPage = 20;

    /**
     * Permite identificar los registros excluidos de la consulta
     * @var type
     */
    public $notIn = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ciudad_id'], 'integer'],
            [['nombre', 'notIn'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (isset($params['per-page']))
        {
            $this->perPage = $params['per-page'];
        }

        if (isset($params['notIn']))
        {
            $params['notIn'] = \yii\helpers\Json::decode($params['notIn']);
            $this->notIn     = is_array($params['notIn']) ? array_values($params['notIn']) : [
                    ];
        }

        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->perPage,
            ],
            /*'sort' => [
                'defaultOrder' => [                    
                    'nombre' => SORT_ASC, 
                ]
            ],*/
        ]);

        $this->setAttributes($params);


        if ($this->perPage == 0)
        {
            $dataProvider->pagination = false;
        }

        $query->andFilterWhere(['not in', 'ciudad_id', $this->notIn]);

        $query->andFilterWhere([
            'ciudad_id' => $this->ciudad_id,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
    
    /**
     * Campos disponibles en el servicio
     * @return type
     */
    public function fields()
    {
        return [
            'ciudad_id',
            'nombre',
            
        ];
    }
}
