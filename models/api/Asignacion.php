<?php

namespace app\models\api;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\base\Asignacion as AsignacionModel;

/**
 * Asignacion represents the model behind the search form about `app\models\base\Asignacion`.
 */
class Asignacion extends AsignacionModel
{

    /**
     * Datos activos
     */
    public $active = 'Y';

    /**
     * Permite establecer el número de registros a ser mostrados por pagina
     * @var type
     */
    public $perPage = 20;

    /**
     * Permite identificar los registros excluidos de la consulta
     * @var type
     */
    public $notIn = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['asignacion_id', 'vehiculo_id', 'conductor_id'], 'integer'],
            [['notIn'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (isset($params['per-page']))
        {
            $this->perPage = $params['per-page'];
        }

        if (isset($params['notIn']))
        {
            $params['notIn'] = \yii\helpers\Json::decode($params['notIn']);
            $this->notIn     = is_array($params['notIn']) ? array_values($params['notIn']) : [
                    ];
        }

        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->perPage,
            ],
            /*'sort' => [
                'defaultOrder' => [                    
                    'nombre' => SORT_ASC, 
                ]
            ],*/
        ]);

        $this->setAttributes($params);

        if ($this->perPage == 0)
        {
            $dataProvider->pagination = false;
        }

        $query->andFilterWhere(['not in', 'asignacion_id', $this->notIn]);

        $query->andFilterWhere([
            'asignacion_id' => $this->asignacion_id,
            'vehiculo_id' => $this->vehiculo_id,
            'conductor_id' => $this->conductor_id,
        ]);

        return $dataProvider;
    }

}
