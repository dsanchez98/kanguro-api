<?php

namespace app\models\api;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\base\Persona as PersonaModel;

/**
 * Persona represents the model behind the search form about `app\models\Persona`.
 */
class Persona extends PersonaModel
{

    /**
     * Datos activos
     */
    public $active = 'Y';

    /**
     * Permite establecer el número de registros a ser mostrados por pagina
     * @var type
     */
    public $perPage = 20;

    /**
     * Permite identificar los registros excluidos de la consulta
     * @var type
     */
    public $notIn = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['persona_id', 'direccion', 'ciudad_id'], 'integer'],
            [['numero_documento', 'primer_nombre', 'segundo_nombre', 'apellidos', 'telefono', 'notIn'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (isset($params['per-page']))
        {
            $this->perPage = $params['per-page'];
        }

        if (isset($params['notIn']))
        {
            $params['notIn'] = \yii\helpers\Json::decode($params['notIn']);
            $this->notIn     = is_array($params['notIn']) ? array_values($params['notIn']) : [
                    ];
        }

        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->perPage,
            ],
            /*'sort' => [
                'defaultOrder' => [                    
                    'nombre' => SORT_ASC, 
                ]
            ],*/
        ]);

        $this->setAttributes($params);

        if ($this->perPage == 0)
        {
            $dataProvider->pagination = false;
        }

        $query->andFilterWhere(['not in', 'persona_id', $this->notIn]);

        $query->andFilterWhere([
            'persona_id' => $this->persona_id,
            'direccion' => $this->direccion,
            'ciudad_id' => $this->ciudad_id,
        ]);

        $query->andFilterWhere(['like', 'numero_documento', $this->numero_documento])
            ->andFilterWhere(['like', 'primer_nombre', $this->primer_nombre])
            ->andFilterWhere(['like', 'segundo_nombre', $this->segundo_nombre])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'telefono', $this->telefono]);

        return $dataProvider;
    }
    
}
