<?php

namespace app\models\base;

use app\models\api\Asignacion;
use app\models\api\Persona;
use Yii;

/**
 * Éste es el modelo para la tabla "vehiculo".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $vehiculo_id 
 * @property string $placa 
 * @property string $color 
 * @property string $marca 
 * @property string $tipo_vehiculo 
 * @property integer $propietario_id 
 * @property Asignacion[] $asignacions Datos relacionados con modelo "Asignacion"
 * @property Persona $propietario Datos relacionados con modelo "Persona"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class Vehiculo extends \ticmakers\base\base\Model
{
    /**
     * Datos activos
     */
    public $active = 'Y';
    
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'vehiculo';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['placa', 'color', 'marca', 'tipo_vehiculo', 'propietario_id'], 'required'],
            [['propietario_id'], 'integer'],
            [['placa'], 'string', 'max' => 6],
            [['color'], 'string', 'max' => 20],
            [['marca'], 'string', 'max' => 45],
            [['tipo_vehiculo'], 'string', 'max' => 3],
            [['placa'], 'unique'],
            [['propietario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::class, 'targetAttribute' => ['propietario_id' => 'persona_id']],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'vehiculo_id' => Yii::t('app', 'Código'),
            'placa' => Yii::t('app', 'Placa'),
            'color' => Yii::t('app', 'Color'),
            'marca' => Yii::t('app', 'Marca'),
            'tipo_vehiculo' => Yii::t('app', 'Tipo Vehiculo'),
            'propietario_id' => Yii::t('app', 'Propietario'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'vehiculo_id' => Yii::t('app', ''),
            'placa' => Yii::t('app', ''),
            'color' => Yii::t('app', ''),
            'marca' => Yii::t('app', ''),
            'tipo_vehiculo' => Yii::t('app', ''),
            'propietario_id' => Yii::t('app', ''),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "Asignacions".
     *
     * @return \app\models\base\Asignacions
     */
    public function getAsignacions()
    {
        $query = $this->hasMany(Asignacion::class, ['vehiculo_id' => 'vehiculo_id']);
		$query->andWhere([self::STATUS_COLUMN => self::STATUS_ACTIVE]);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "Propietario".
     *
     * @return \app\models\base\Propietario
     */
    public function getPropietario()
    {
        return $this->hasOne(Persona::class, ['persona_id' => 'propietario_id']);
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|Vehiculo[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'vehiculo_id', self::getNameFromRelations());
        }
        return $query;
    }

        /**
     * Configuración de los comportamientos por defecto de la aplicación
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Campos disponibles en el servicio
     * @return type
     */
    public function fields()
    {
        return [
            'vehiculo_id',
            'placa',
            'color',
            'marca',
            'tipo_vehiculo',
            'propietario_id',
            'propietario'
        ];
    }
}
