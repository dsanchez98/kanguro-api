<?php

namespace app\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "asignacion".
 * 
 *
 * @package app\models\base\models
 *
 * @property integer $asignacion_id 
 * @property integer $vehiculo_id 
 * @property integer $conductor_id 
 * @property Vehiculo $vehiculo Datos relacionados con modelo "Vehiculo"
 * @property Persona $conductor Datos relacionados con modelo "Persona"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class Asignacion extends \ticmakers\core\base\Model
{

    /**
     * Datos activos
     */
    public $active = 'Y';


    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'asignacion';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['vehiculo_id', 'unique', 'targetAttribute' => ['vehiculo_id', 'conductor_id'], 'message'=>"La combinación vehículo conductor ya ha sido tomada, por favor verifique"],
            [['vehiculo_id', 'conductor_id'], 'required'],
            [['vehiculo_id', 'conductor_id'], 'integer'],
            [['vehiculo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['vehiculo_id' => 'vehiculo_id']],
            [['conductor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['conductor_id' => 'persona_id']],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'asignacion_id' => Yii::t('app', 'Código'),
            'vehiculo_id' => Yii::t('app', 'Vehiculo'),
            'conductor_id' => Yii::t('app', 'Conductor'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'asignacion_id' => Yii::t('app', ''),
            'vehiculo_id' => Yii::t('app', ''),
            'conductor_id' => Yii::t('app', ''),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "Vehiculo".
     *
     * @return \app\models\base\Vehiculo
     */
    public function getVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['vehiculo_id' => 'vehiculo_id']);
    }

    /**
     * Definición de la relación con el modelo "Conductor".
     *
     * @return \app\models\base\Conductor
     */
    public function getConductor()
    {
        return $this->hasOne(Persona::className(), ['persona_id' => 'conductor_id']);
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|Asignacion[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'asignacion_id', self::getNameFromRelations());
        }
        return $query;
    }

    /**
     * Configuración de los comportamientos por defecto de la aplicación
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

        
    /**
     * Campos disponibles en el servicio
     * @return type
     */
    public function fields()
    {
        return [
            'asignacion_id',
            'vehiculo_id',
            'conductor_id',
            'vehiculo',
            'conductor'
        ];
    }
}
