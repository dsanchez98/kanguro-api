<?php

namespace app\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "ciudad".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $ciudad_id 
 * @property string $nombre 
 * @property Persona[] $personas Datos relacionados con modelo "Persona"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class Ciudad extends \ticmakers\core\base\Model
{
    /**
     * Datos activos
     */
    public $active = 'Y';
    
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'ciudad';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 30],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'ciudad_id' => Yii::t('app', 'Código'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'ciudad_id' => Yii::t('app', ''),
            'nombre' => Yii::t('app', ''),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "Personas".
     *
     * @return \app\models\base\Personas
     */
    public function getPersonas()
    {
        $query = $this->hasMany(Persona::className(), ['ciudad_id' => 'ciudad_id']);
		$query->andWhere([self::STATUS_COLUMN => self::STATUS_ACTIVE]);
		return $query;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|Ciudad[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'ciudad_id', self::getNameFromRelations());
        }
        return $query;
    }

    /**
     * Configuración de los comportamientos por defecto de la aplicación
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }
}
