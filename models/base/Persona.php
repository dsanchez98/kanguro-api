<?php

namespace app\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "persona".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $persona_id 
 * @property string $numero_documento 
 * @property string $primer_nombre 
 * @property string $segundo_nombre 
 * @property string $apellidos 
 * @property string $direccion 
 * @property string $telefono 
 * @property integer $ciudad_id 
 * @property Asignacion[] $asignacions Datos relacionados con modelo "Asignacion"
 * @property Ciudad $ciudad Datos relacionados con modelo "Ciudad"
 * @property Vehiculo[] $vehiculos Datos relacionados con modelo "Vehiculo"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class Persona extends \ticmakers\core\base\Model
{
    /**
     * Datos activos
     */
    public $active = 'Y';

    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'persona';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['numero_documento', 'unique'],
            [['numero_documento', 'primer_nombre', 'apellidos', 'direccion', 'telefono', 'ciudad_id'], 'required'],
            [['ciudad_id'], 'integer'],
            [['numero_documento'], 'string', 'max' => 16],
            [['primer_nombre', 'segundo_nombre', 'telefono'], 'string', 'max' => 20],
            [['apellidos'], 'string', 'max' => 30],
            [['ciudad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ciudad::class, 'targetAttribute' => ['ciudad_id' => 'ciudad_id']],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'persona_id' => Yii::t('app', 'Código'),
            'numero_documento' => Yii::t('app', 'Número de documento'),
            'primer_nombre' => Yii::t('app', 'Primer nombre'),
            'segundo_nombre' => Yii::t('app', 'Segundo nombre'),
            'apellidos' => Yii::t('app', 'Apellidos'),
            'direccion' => Yii::t('app', 'Direccion'),
            'telefono' => Yii::t('app', 'Telefono'),
            'ciudad_id' => Yii::t('app', 'Ciudad'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'persona_id' => Yii::t('app', ''),
            'numero_documento' => Yii::t('app', ''),
            'primer_nombre' => Yii::t('app', ''),
            'segundo_nombre' => Yii::t('app', ''),
            'apellidos' => Yii::t('app', ''),
            'direccion' => Yii::t('app', ''),
            'telefono' => Yii::t('app', ''),
            'ciudad_id' => Yii::t('app', ''),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "Asignacions".
     *
     * @return \app\models\base\Asignacions
     */
    public function getAsignacions()
    {
        $query = $this->hasMany(Asignacion::class, ['conductor_id' => 'persona_id']);
		$query->andWhere([self::STATUS_COLUMN => self::STATUS_ACTIVE]);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "Ciudad".
     *
     * @return \app\models\base\Ciudad
     */
    public function getCiudad()
    {
        return $this->hasOne(Ciudad::class, ['ciudad_id' => 'ciudad_id']);
    }

    /**
     * Definición de la relación con el modelo "Vehiculos".
     *
     * @return \app\models\base\Vehiculos
     */
    public function getVehiculos()
    {
        $query = $this->hasMany(Vehiculo::class, ['propietario_id' => 'persona_id']);
		$query->andWhere([self::STATUS_COLUMN => self::STATUS_ACTIVE]);
		return $query;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|Persona[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'persona_id', self::getNameFromRelations());
        }
        return $query;
    }

    /**
     * Configuración de los comportamientos por defecto de la aplicación
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Campos disponibles en el servicio
     * @return type
     */
    public function fields()
    {
        return [
            'persona_id',
            'numero_documento',
            'primer_nombre',
            'segundo_nombre',
            'apellidos',
            'direccion',
            'telefono',
            'ciudad_id',
            'ciudad'
        ];
    }
}
