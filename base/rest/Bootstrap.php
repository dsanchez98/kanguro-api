<?php

namespace app\base\rest;

use Yii;

use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package app\base\rest
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 */
class Bootstrap implements BootstrapInterface
{
    
    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        Yii::$container->set(\ticmakers\core\rest\Controller::class, \app\base\rest\Controller::class);
    }

}