<?php

namespace app\base\rest;

use ticmakers\core\rest\ActiveController as RestActiveController;
use Yii;
use yii\web\Response;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\ActiveController as YiiRestActiveController;

/**
 * ActiveController Implementa las Acciones REST disponibles para los controladores del módulo Api.
 *
 * @package ticmakers
 * @subpackage rest/controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class ActiveController extends RestActiveController
{

    /**
     * La configuración para crear el serializador para el formato de respuesta.
     * @var array
     */
    public $serializer = 'yii\rest\Serializer';

    /**
     * Retorna la lista de behaviors que el controlador implementa
     *
     * @return array
     */
    public function behaviors()
    {
        $this->cors();

        $behaviors                                                     = YiiRestActiveController::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;

        $behaviors['authenticator'] = [
            'class'       => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
            ],
        ];
        return $behaviors;
    }

    /**
     * Configuración de CORS
     */
    public function cors()
    {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, OPTIONS, DELETE");
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
            exit(0);
        }
    }

    /**
     * Retorna la parametrización para las acciones por defecto
     *
     * @return array
     */
    public function actions()
    {
        $actions            = parent::actions();
        $actions['delete']  = [
            'class'       => 'app\base\rest\actions\DeleteAction',
            'modelClass'  => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];
        return $actions;
    }

    /**
     * Declara los verbos HTTP permitidos.
     *
     * @return array
     */
    public function verbs()
    {
        $verbs           = parent::verbs();
        $verbs["index"]  = ['GET'];
        $verbs["update"] = ['PUT', 'PATCH', 'POST'];
        $verbs["delete"] = ['DELETE'];
        return $verbs;
    }

}
