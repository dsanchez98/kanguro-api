<?php

namespace app\base\rest\actions;

use ticmakers\core\rest\actions\DeleteAction as ActionsDeleteAction;
use Yii;
use yii\web\ServerErrorHttpException;
use yii\rest\Action;

/**
 * DeleteAction Acción eliminar disponible en el API REST.
 *
 * @package app\base\rest\actions
 * @category Action
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class DeleteAction extends ActionsDeleteAction
{

    /**
     * Deletes a model.
     * @param mixed $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {
        if (!empty($id) && is_numeric($id))
        {
            $model = $this->findModel($id);

            if ($this->checkAccess)
            {
                call_user_func($this->checkAccess, $this->id, $model);
            }

           

            if ($model->delete() === false)
            {
                throw new ServerErrorHttpException(Yii::t('app',
                                                          'Failed to delete the object for unknown reason.'));
            }

            return [
                'name'    => Yii::t('app', 'Successful operation'),
                'message' => Yii::t('app',
                                    'The record has been removed successfully.'),
                'code'    => 0,
                'status'  => 200,
                'type'    => '',
            ];
        }
        else
        {
            throw new \InvalidArgumentException(Yii::t('app', 'Invalid item ID'));
        }
    }

}
