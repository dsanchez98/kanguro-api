<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name'=>'Kanguro',
    'basePath' => dirname(__DIR__),
    'layoutPath' => '@app/themes/mintos/views/layouts/',
    'language' => 'es-CO',
    'bootstrap' => [
        'log',
        'app\base\rest\Bootstrap',
        'app\themes\mintos\Bootstrap',
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@themes' => '@app/themes/',
        '@theme' => '@app/themes/mintos/',
        '@webroot' => '@app/web',
        '@web' => '@app/web'
    ],
    'components' => [
        'view' => [
            'class' => \ticmakers\core\base\View::class,
            'theme' => [
                'class' => \app\themes\mintos\Theme::class,
                'basePath' => '@theme',
                'baseUrl' => '@web/themes/mintos',
                'mainLayout' => 'main',
                'imagesBaseUrl' => '@web/images',
                'pathMap' => [
                    '@app/views' => [
                        '@app/views',
                        '@theme/views',
                    ],
                ]
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '2_ZSgJdRafZLyKbqhJRpRueTzcE2QBTp',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'loginUrl'=>['site/index']
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'asignacion',
                        'ciudad',
                        'persona',
                        'vehiculo'
                    ],
                    'pluralize' => false
                ]
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
