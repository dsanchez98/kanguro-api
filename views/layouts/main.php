<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use ticmakers\core\base\Modal;
use yii\bootstrap4\Html;

AppAsset::register($this);
$this->registerJs("
    window.TEXT_EMPTY = '" . Yii::$app->strings::getTextEmpty() . "';
    let loadModalElements = document.getElementsByClassName('load-modal');
    for (let element of loadModalElements) {
        element.addEventListener('click', function (evt) {
            evt.preventDefault();
        });
    }
    bootbox.setLocale('" . Yii::$app->language . "');
    " . Yii::$app->message::getMessagesJS() . "
", \yii\web\View::POS_LOAD);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->


    <!-- HK Wrapper -->
    <div class="hk-wrapper hk-vertical-nav">

        <!-- Top Navbar -->
        <?php //$this->beginContent('@theme/views/layouts/sections/top-nav.php');
        ?>
        <?php //$this->endContent();
        ?>
        <!-- /Top Navbar -->

        <!-- Vertical Nav -->
        <?php //$this->beginContent('@theme/views/layouts/sections/vertical-nav.php');
        ?>
        <?php //$this->endContent();
        ?>
        <!-- /Vertical Nav -->

        <!-- Setting Panel -->
        <?php //$this->beginContent('@theme/views/layouts/sections/setting-panel.php');
        ?>
        <?php //$this->endContent();
        ?>
        <!-- /Setting Panel -->

        <!-- Main Content -->
        <div class="hk-pg-wrapper">

            <!-- Container -->
            <div class="container pt-30">
                <?= $content ?>
            </div>
            <!-- /Container -->

            <!-- Footer -->
            <?php $this->beginContent('@theme/views/layouts/sections/footer.php'); ?>
            <?php $this->endContent(); ?>
            <!-- /Footer -->
        </div>
        <!-- /Main Content -->

    </div>
    <!-- /HK Wrapper -->
    <?php Modal::begin([
        'id' => 'default-modal',
        'title' => Yii::t('app', '{title}'),
        'size' => Modal::SIZE_EXTRA_LARGE,
        'options' => [
            'style' => 'display: none;',
            'tabindex' => false,
        ],
        'clientOptions' => [
            'backdrop' => 'static',
            'keyboard' => false,
        ],
    ]);
    Modal::end();
    ?>
    <div id="move-modal"></div>
    <?php $this->endBody() ?>
</body>
<?php $this->endPage() ?>